package com.popwallet.popbuilder.models;

import com.google.common.collect.ImmutableList;
import lombok.Builder;
import lombok.Singular;
import lombok.Value;
import lombok.With;

@Value
@Builder
public class BrandLombok {
    int id;
    String name;
    @With
    String description;
    @Singular
    ImmutableList<PopCardLombok> popCards;
}
