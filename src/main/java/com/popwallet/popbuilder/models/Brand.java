package com.popwallet.popbuilder.models;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;

public class Brand {

    private final int id;
    private final String name;
    private final String description;
    private final List<PopCard> popCards;

    private Brand(int id, String name, String description, List<PopCard> popCards) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.popCards = List.copyOf(popCards);
    }

    public static BrandBuilder builder() {
        return new BrandBuilder();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<PopCard> getPopCards() {
        return popCards;
    }

    public static class BrandBuilder {

        private Integer id;
        private String name;
        private String description;
        private List<PopCard> popCards;

        private BrandBuilder () {}

        public BrandBuilder id(int id) {
            this.id = id;
            return this;
        }

        public BrandBuilder name(String name) {
            this.name = name;
            return this;
        }

        public BrandBuilder description(String description) {
            this.description = description;
            return this;
        }

        public BrandBuilder popCard(PopCard popCard) {
            if (CollectionUtils.isNotEmpty(this.popCards)) {
                this.popCards.add(popCard);
            } else {
                this.popCards = new ArrayList<>() {{ add(popCard); }};
            }
            return this;
        }

        public BrandBuilder popCards(List<PopCard> popCards) {
            if (CollectionUtils.isNotEmpty(this.popCards)) {
                this.popCards.addAll(popCards);
            } else {
                this.popCards = List.copyOf(popCards);
            }
            return this;
        }

        public Brand build() {
            if (isNull(this.id) || isNull(this.name)) {
                throw new RuntimeException();
            }
            return new Brand(this.id, this.name, this.description, this.popCards);
        }
    }
}
