package com.popwallet.popbuilder.models;

import static java.util.Objects.isNull;

public class PopCard {

    private final int id;
    private final String title;
    private final String type;
    private final String description;

    private PopCard(int id, String title, String type, String description) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.description = description;
    }

    public static PopCardBuilder builder() {
        return new PopCardBuilder();
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public static class PopCardBuilder {

        private Integer id;
        private String title;
        private String type;
        private String description;

        private PopCardBuilder() {}

        public PopCardBuilder id(int id) {
            this.id = id;
            return this;
        }

        public PopCardBuilder title(String title) {
            this.title = title;
            return this;
        }

        public PopCardBuilder type(String type) {
            this.type = type;
            return this;
        }

        public PopCardBuilder description(String description) {
            this.description = description;
            return this;
        }

        public PopCard build() {
            if (isNull(this.id)) {
                throw new RuntimeException();
            }
            return new PopCard(this.id, this.title, this.type, this.description);
        }
    }
}
