package com.popwallet.popbuilder.models;

import lombok.Builder;

@Builder
public class PopCardLombok {
    private int id;
    private String title;
    private String type;
    private String description;
}
