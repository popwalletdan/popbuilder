package com.popwallet.popbuilder.services;

import com.popwallet.popbuilder.models.Brand;
import com.popwallet.popbuilder.models.BrandLombok;
import com.popwallet.popbuilder.models.PopCard;
import com.popwallet.popbuilder.models.PopCardLombok;
import lombok.val;

import java.util.ArrayList;

public class PopCardService {

    public PopCard generatePopCard(int id, String title, boolean addDescription) {

        final PopCard.PopCardBuilder popCardBuilder = PopCard.builder();

        popCardBuilder.id(id)
            .title(title)
            .type("BRAND");

        if (addDescription) {
            popCardBuilder.description("A Builder Pattern Demo");
        }

        final PopCard popCard = popCardBuilder.build();
        return popCard;
    }

    public PopCardLombok generatePopCardLombok(int id) {

        final PopCardLombok popCardLombok = PopCardLombok.builder()
            .id(id)
            .title("Lombok Builder Card")
            .description("Built with Lombok")
            .type("BRAND")
            .build();


        return popCardLombok;
    }

    public Brand generateBrand() {

        final var brandBuilder = Brand.builder();

        brandBuilder.id(1)
            .name("Popwallet")
            .description("A mobile wallet marketing company")
            .popCard(generatePopCard(1, "Card 1", true));

        final var popCards = new ArrayList<PopCard>() {{
            add(generatePopCard(2, "Card 2", false));
            add(generatePopCard(3, "Card 3", false));
        }};

        brandBuilder.popCards(popCards);

        return brandBuilder.build();
    }

    public BrandLombok generateBrandLombok() {

        // val == final var
        val lombokBrandBuilder = BrandLombok.builder();

        lombokBrandBuilder.id(1)
            .name("Popwallet")
            .description("A mobile wallet marketing company")
            .popCard(generatePopCardLombok(1));

        val popCards = new ArrayList<PopCardLombok>() {{
            add(generatePopCardLombok(2));
            add(generatePopCardLombok(3));
        }};

        lombokBrandBuilder.popCards(popCards);

        return lombokBrandBuilder.build();
    }
}
