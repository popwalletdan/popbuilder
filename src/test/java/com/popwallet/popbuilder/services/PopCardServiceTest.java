package com.popwallet.popbuilder.services;

import com.popwallet.popbuilder.models.Brand;
import com.popwallet.popbuilder.models.PopCard;
import lombok.val;
import org.junit.Before;
import org.junit.Test;

import static java.util.Objects.nonNull;
import static org.junit.Assert.*;

public class PopCardServiceTest {

    private PopCardService popCardService;

    @Before
    public void setUp() throws Exception {
        this.popCardService = new PopCardService();
    }

    @Test
    public void generatePopCard_whenAddDescriptionIsTrue_thenReturnPopCardWithDescription() {
        final PopCard result = popCardService.generatePopCard(1,"Builder Card", true);

        assertTrue(nonNull(result.getDescription()));
    }

    @Test
    public void generatePopCard_whenAddDescriptionIsFalse_thenDoNotReturnDescriptionOnPopCard() {
        final PopCard result = popCardService.generatePopCard(1,"Builder Card",false);

        assertFalse(nonNull(result.getDescription()));
    }

    @Test
    public void generateBrand_whenCalled_shouldReturn3PopCards() {

        final Brand result = popCardService.generateBrand();

        for (PopCard popCard : result.getPopCards()) {

            final var title = switch(popCard.getId()) {
                case 1 -> "Card 1";
                case 2 -> "Card 2";
                case 3 -> "Card 3";
                default -> throw new IllegalArgumentException();
            };

            assertEquals(title, popCard.getTitle());
        }
    }
}